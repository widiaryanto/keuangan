package com.widi.aryanto.keuangan.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.widi.aryanto.keuangan.data.local.db.AdmissionFee
import com.widi.aryanto.keuangan.data.repository.Repository
import kotlinx.coroutines.launch

class AppViewModel(private val repository: Repository) : ViewModel() {

    val allAdmissionFee: LiveData<List<AdmissionFee>> = repository.allAdmissionFee.asLiveData()

    fun insert(admissionFee: AdmissionFee) = viewModelScope.launch {
        repository.insert(admissionFee)
    }
}

class ViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AppViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return AppViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}