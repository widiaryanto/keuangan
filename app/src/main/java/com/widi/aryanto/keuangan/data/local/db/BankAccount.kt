package com.widi.aryanto.keuangan.data.local.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Rekening")
class BankAccount (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "RekeningID")
    val id: Int? = null,

    @ColumnInfo(name = "NamaBank")
    val bankName: String? = "",

    @ColumnInfo(name = "NomorRekening")
    val bankAccount: String? = "",

    @ColumnInfo(name = "AtasNama")
    val accountHolder: String? = ""

)