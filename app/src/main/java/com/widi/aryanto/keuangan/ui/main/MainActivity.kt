package com.widi.aryanto.keuangan.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.widi.aryanto.keuangan.ui.add.AddActivity
import com.widi.aryanto.keuangan.App
import com.widi.aryanto.keuangan.viewmodel.AppViewModel
import com.widi.aryanto.keuangan.viewmodel.ViewModelFactory
import com.widi.aryanto.keuangan.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: AppViewModel by viewModels {
        ViewModelFactory((application as App).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerview.layoutManager = LinearLayoutManager(this@MainActivity)
        val adapter = Adapter()
        binding.recyclerview.adapter = adapter

        viewModel.allAdmissionFee.observe(this@MainActivity) { words ->
            words?.let { adapter.submitList(it) }
        }

        binding.fab.setOnClickListener {
            val intent = Intent(this@MainActivity, AddActivity::class.java)
            startActivity(intent)
        }
    }
}