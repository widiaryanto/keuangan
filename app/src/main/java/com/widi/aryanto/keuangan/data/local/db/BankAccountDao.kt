package com.widi.aryanto.keuangan.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface BankAccountDao {

    @Query("SELECT * FROM Rekening ORDER BY RekeningID ASC")
    fun getBankAccount(): Flow<List<BankAccount>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(bank: BankAccount)

    @Query("DELETE FROM Rekening")
    suspend fun deleteAll()
}