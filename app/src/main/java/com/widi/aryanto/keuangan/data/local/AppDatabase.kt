package com.widi.aryanto.keuangan.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.widi.aryanto.keuangan.data.local.db.AdmissionFee
import com.widi.aryanto.keuangan.data.local.db.AdmissionFeeDao

@Database(entities = [AdmissionFee::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun dao(): AdmissionFeeDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(
            context: Context
        ): AppDatabase {
            val migration2to3 = object : Migration(2, 3) {
                override fun migrate(database: SupportSQLiteDatabase) {
                    database.execSQL("CREATE TABLE `Rekening` (`RekeningID` INTEGER, `NamaBank` TEXT, `NomorRekening` TEXT, `AtasNama` TEXT, PRIMARY KEY(`RekeningID`))")
                    database.execSQL("ALTER TABLE UangMasuk ADD COLUMN RekeningID TEXT")
                }
            }
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "nutapos_database"
                ).addMigrations(migration2to3).build()
                INSTANCE = instance
                instance
            }
        }
    }
}