package com.widi.aryanto.keuangan.ui.add

import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.widi.aryanto.keuangan.App
import com.widi.aryanto.keuangan.viewmodel.AppViewModel
import com.widi.aryanto.keuangan.R
import com.widi.aryanto.keuangan.viewmodel.ViewModelFactory
import com.widi.aryanto.keuangan.data.local.db.AdmissionFee
import com.widi.aryanto.keuangan.databinding.ActivityAddBinding
import com.widi.aryanto.keuangan.utils.DateUtils


class AddActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddBinding
    private val viewModel: AppViewModel by viewModels {
        ViewModelFactory((application as App).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = getString(R.string.add_data)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.buttonSave.setOnClickListener {
            if (TextUtils.isEmpty(binding.editFrom.text) && TextUtils.isEmpty(binding.editTotal.text)) {
                Toast.makeText(
                    applicationContext,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show()
            } else {
                val test = AdmissionFee(
                    from = binding.editFrom.text.toString(),
                    description = binding.editDescription.text.toString(),
                    total = binding.editTotal.text.toString().toInt(),
                    date = DateUtils().getCurrentDate(),
                    number = DateUtils().getFormattingNumber()
                )
                viewModel.insert(test)
                Toast.makeText(
                    applicationContext,
                    R.string.data_saved,
                    Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}