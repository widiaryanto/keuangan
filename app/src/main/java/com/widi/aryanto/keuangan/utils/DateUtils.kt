package com.widi.aryanto.keuangan.utils

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class DateUtils {

    fun getCurrentDate(): String {
        val c: Date = Calendar.getInstance().time
        val df = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
        return df.format(c)
    }

    fun getFormattingNumber(): String {
        val c: Date = Calendar.getInstance().time
        val year = SimpleDateFormat("yy", Locale.getDefault()).format(c)
        val month = SimpleDateFormat("MM", Locale.getDefault()).format(c)
        val day = SimpleDateFormat("dd", Locale.getDefault()).format(c)
        return "UM/$year$month$day/1"
    }

}