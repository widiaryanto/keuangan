package com.widi.aryanto.keuangan.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.widi.aryanto.keuangan.R
import com.widi.aryanto.keuangan.data.local.db.AdmissionFee
import com.widi.aryanto.keuangan.databinding.AdmissionFeeItemBinding

class Adapter : ListAdapter<AdmissionFee, Adapter.ViewHolder>(DiffComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = AdmissionFeeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(parent.context, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    class ViewHolder(
        private val context: Context,
        private var binding: AdmissionFeeItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: AdmissionFee) {
            binding.tvItemId.text = context.getString(R.string.id) + data.id.toString()
            binding.tvItemForm.text = data.from
            binding.tvItemDescription.text = data.description
            binding.tvItemTotal.text = data.total.toString()
            binding.tvItemNumber.text = data.number.toString()
            binding.tvItemDate.text = data.date.toString()
        }
    }

    class DiffComparator : DiffUtil.ItemCallback<AdmissionFee>() {
        override fun areItemsTheSame(oldItem: AdmissionFee, newItem: AdmissionFee): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: AdmissionFee, newItem: AdmissionFee): Boolean {
            return oldItem.id == newItem.id
        }
    }
}