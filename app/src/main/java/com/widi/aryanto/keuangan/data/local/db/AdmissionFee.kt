package com.widi.aryanto.keuangan.data.local.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UangMasuk")
class AdmissionFee (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "UangMasukID")
    val id: Int? = null,

    @ColumnInfo(name = "TerimaDari")
    val from: String,

    @ColumnInfo(name = "Keterangan")
    val description: String? = "",

    @ColumnInfo(name = "Jumlah")
    val total: Int,

    @ColumnInfo(name = "Tanggal")
    val date: String? = "",

    @ColumnInfo(name = "Nomor")
    val number: String? = "",

    @ColumnInfo(name = "RekeningID")
    val bankAccount: String? = ""

)