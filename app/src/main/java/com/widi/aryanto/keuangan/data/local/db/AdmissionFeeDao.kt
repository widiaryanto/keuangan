package com.widi.aryanto.keuangan.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface AdmissionFeeDao {

    @Query("SELECT * FROM UangMasuk ORDER BY UangMasukID ASC")
    fun getAdmissionFee(): Flow<List<AdmissionFee>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word: AdmissionFee)

    @Query("DELETE FROM UangMasuk")
    suspend fun deleteAll()
}