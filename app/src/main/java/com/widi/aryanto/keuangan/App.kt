package com.widi.aryanto.keuangan

import android.app.Application
import com.widi.aryanto.keuangan.data.local.AppDatabase
import com.widi.aryanto.keuangan.data.repository.Repository

class App : Application() {
    private val database by lazy { AppDatabase.getDatabase(this) }
    val repository by lazy { Repository(database.dao()) }
}