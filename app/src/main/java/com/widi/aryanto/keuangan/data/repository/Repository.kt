package com.widi.aryanto.keuangan.data.repository

import androidx.annotation.WorkerThread
import com.widi.aryanto.keuangan.data.local.db.AdmissionFee
import com.widi.aryanto.keuangan.data.local.db.AdmissionFeeDao
import kotlinx.coroutines.flow.Flow

class Repository(private val dao: AdmissionFeeDao) {

    val allAdmissionFee: Flow<List<AdmissionFee>> = dao.getAdmissionFee()

    @WorkerThread
    suspend fun insert(admissionFee: AdmissionFee) {
        dao.insert(admissionFee)
    }
}